# Simple Reminder

this is a Simple reminder app that was developed to test out awesome_flutter_notification and date_time_picker plugin

## Plugins
### Awesome Flutter Notification
-> https://pub.dev/packages/awesome_notifications

### Flutter DateTime Picker
-> https://pub.dev/packages/flutter_datetime_picker


Use the following command to import plugin 
```
flutter pub get
```
