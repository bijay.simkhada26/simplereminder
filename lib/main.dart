import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:notification_test/Home/home.dart';

void main() {
  AwesomeNotifications().initialize('', [
    NotificationChannel(
        channelKey: 'basic_channel',
        channelName: 'Basic Notification',
        defaultColor: Colors.brown,
        importance: NotificationImportance.High,
        channelShowBadge: true),
    NotificationChannel(
        channelKey: 'reminder_channel',
        channelName: 'Remainder Channel',
        defaultColor: Colors.blue,
        importance: NotificationImportance.High,
        channelShowBadge: true),
  ]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Notification Demo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: HomeScreen(),
    );
  }
}
