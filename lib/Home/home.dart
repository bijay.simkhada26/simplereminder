import 'dart:io';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:notification_test/Notifications/notificationShow.dart';
import '../Notifications/Notifications.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DateTime? date;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      if (!isAllowed) {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: const Text('Allow Notifications'),
                  content: const Text(
                      'Our Application would like to send you notifications'),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text("Don't Allow")),
                    TextButton(
                        onPressed: () {
                          AwesomeNotifications()
                              .requestPermissionToSendNotifications()
                              .then((_) => Navigator.pop(context));
                        },
                        child: const Text("Allow"))
                  ],
                ));
      }
    });

    AwesomeNotifications().actionStream.listen((event) {
      if (event.channelKey == 'basic_channel' && Platform.isIOS) {
        AwesomeNotifications().getGlobalBadgeCounter().then(
            (value) => AwesomeNotifications().setGlobalBadgeCounter(value - 1));
      }
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (_) => ShowScreen(event.body)),
          (route) => route.isFirst);
    });
  }

  @override
  void dispose() {
    AwesomeNotifications().actionSink.close();
    super.dispose();
  }

  late String title;
  late String body;
  _selectDate(BuildContext context) async {
    final DateTime? dateSelected = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2024));
    if (dateSelected != null) {
      print(dateSelected);
      final TimeOfDay? timeSelected =
          await showTimePicker(context: context, initialTime: TimeOfDay.now());
      if (timeSelected != null) {
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  content: Container(
                    height: 300,
                    width: 300,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text('Reminder', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                        TextField(
                          decoration: const InputDecoration(
                              label: Text('Title'), hintText: 'Title here'),
                          onChanged: (value) {
                            setState(() {
                              title = value;
                            });
                          },
                        ),
                        TextField(
                          decoration: const InputDecoration(
                              label: Text('Detail'), hintText: 'Detail here'),
                          onChanged: (value) {
                            setState(() {
                              body = value;
                            });
                          },
                        ),
                        ElevatedButton(
                            onPressed: () {
                              reminderNotification(dateSelected, timeSelected, title, body);
                              Navigator.pop(context);
                            },
                            child: const Text('Set Reminder'))
                      ],
                    ),
                  ),
                ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notification Test'),
      ),
      drawer: const Drawer(),
      body: Container(
        padding: const EdgeInsets.all(10),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: dummyNotification,
                child: Text('Send Notifications')),
            ElevatedButton(
                onPressed: () {
                  _selectDate(context);
                },
                child: Text('Create Remainder')),
          ],
        ),
      ),
    );
  }
}
