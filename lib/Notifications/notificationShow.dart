import 'package:flutter/material.dart';

class ShowScreen extends StatelessWidget {
  final String? body;
  ShowScreen(this.body);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dummy Screen'),
        actions: const [Icon(Icons.person_add_alt)],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: Center(
            child: Text(
          'This shows Data $body',
          textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 18
              ),
        )),
      ),
    );
  }
}
