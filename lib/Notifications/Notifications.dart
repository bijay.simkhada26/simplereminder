import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

int createUniqueID() {
  return DateTime.now().microsecondsSinceEpoch.remainder(100000);
}

Future<void> dummyNotification() async {
  print('----Notification----');
  await AwesomeNotifications().createNotification(
      content: NotificationContent(
          id: createUniqueID(),
          title: 'This is Dummy notification',
          channelKey: 'basic_channel',
          body:
              'This notification is sent through the use of AwesomeNotification Plugin for Testing Purpose only'
          // notificationLayout: NotificationLayout.ProgressBar
          ));
}

Future<void> reminderNotification(DateTime date, TimeOfDay? time, String title, String body) async {
  print('-----Creating Notification----');
  print(date);
  print(time);
  AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: createUniqueID(),
        channelKey: 'reminder_channel',
        title: title,
        body: body,
        // locked: true,
        notificationLayout: NotificationLayout.Default,
      ),
      actionButtons: [
        NotificationActionButton(key: 'done', label: 'Mark Done')
      ],
      schedule: NotificationCalendar(
        weekday: date.weekday,
        hour: time?.hour,
        minute: time?.minute,
        second: 0,
        millisecond: 0,
        repeats: true,
      ));
}
